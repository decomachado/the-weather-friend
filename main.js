const OPENWEATHER_API_KEY = "58f2435d3c096d90ec680e70dabf1bde";
var amount_of_cities = 0;

document.getElementById("search-bar").addEventListener('keyup', event => {
    if (event.code === 'Enter') {
        searchForCity();
    }
});

function searchForCity() {
    const input = document.getElementById("search-bar").value;
    const limit = 10;
    axios.get(`http://api.openweathermap.org/geo/1.0/direct?q=${ input }&limit=${ limit }&appid=${ OPENWEATHER_API_KEY }`)
        .then(response => createCitiesList(response.data))
        .catch(err => console.log(err))
}

function createCitiesList(citties_array) {
    var i = 0;
    let div = document.createElement('div');
    div.id = "city-options";
    citties_array.forEach(city => {

        let city_div = document.createElement('div');
        city_div.className = "city";
        city_div.id = `city-wrapper${i}`;
        city_div.addEventListener('click', () => generateWeather(city));

        let city_display = document.createElement('p');
        city_display.className = "city-country";
        city_display.textContent = `${city.name}, ${city.country}`;

        let lat_lon_display = document.createElement('p');
        lat_lon_display.className = "lat-lon";
        lat_lon_display.textContent = `Lat: ${city.lat}, Lon: ${city.lon}`;

        city_div.appendChild(city_display);
        city_div.appendChild(lat_lon_display);

        div.appendChild(city_div);

        i++;
    })
    let wrapper = document.getElementById("choose-city");

    wrapper.appendChild(div);
    document.getElementById("choose-city").style.display = "block";
}


function hideDialog() {
    document.getElementById("choose-city").style.display = "none";
    let div_to_reset = document.getElementById("city-options");
    div_to_reset.remove();
}

function createWeatherContent() {
    let weather_div = document.createElement("div");
    weather_div.className = "weather-wrapper";
    weather_div.id = `weather-wrapper-id${ amount_of_cities }`;

    let div_delete_city = document.createElement("div");
    div_delete_city.className = "close-dialog";
    div_delete_city.addEventListener("click", () => deleteCity(weather_div.id));

    let close_img = document.createElement("img");
    close_img.src = "images/x.png";
    close_img.alt = "Click to delete city";

    div_delete_city.appendChild(close_img);

    let location_heading = document.createElement("h2");
    location_heading.className = "location";

    let div_clear = document.createElement("div");
    div_clear.className = "clear";

    let div_dusk_dawn = document.createElement("div");
    div_dusk_dawn.className = "dusk-dawn";

    let dawn = document.createElement("p");
    let dusk = document.createElement("p");
    dawn.className = "time";
    dusk.className = "time";

    div_dusk_dawn.appendChild(dawn)
    div_dusk_dawn.appendChild(dusk)

    let temp_div = document.createElement("div");
    temp_div.className = "current-temp";

    let img_icon = document.createElement("img");
    img_icon.alt = "Current Weather Icon";
    img_icon.className = "icon";

    weather_div.appendChild(div_delete_city);
    weather_div.appendChild(location_heading);
    weather_div.appendChild(temp_div);
    weather_div.appendChild(div_dusk_dawn);
    weather_div.appendChild(img_icon);

    document.getElementById("content-wrapper").appendChild(weather_div);

    amount_of_cities++;

}

function deleteCity(id) {
    document.getElementById(id).remove();
}

function generateWeather(city) {
    let exclude = "minutely,hourly,daily,alerts";
    let unit = "metric";
    createWeatherContent();
    axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${ city.lat }&lon=${ city.lon }&exclude=${ exclude }&appid=${ OPENWEATHER_API_KEY }&units=${ unit }`)
        .then(response => response.data)
        .then(data => {
            let current = data.current;
            let dawn = new Date(current.sunrise * 1000);
            let dusk = new Date(current.sunset * 1000);

            let current_timezone_offset = getCurrentOffset();
            let offset_in_hours = (current_timezone_offset + (data.timezone_offset / 60)) / 60;
            
            let current_wrapper = document.getElementById(`weather-wrapper-id${ amount_of_cities - 1 }`)
            let dusk_dawn_wrapper = current_wrapper.getElementsByClassName("dusk-dawn")[0];
            let array_of_times = dusk_dawn_wrapper.getElementsByClassName("time");
            array_of_times[0].innerHTML = `Dawn: ${ getRealHours(dawn.getHours(), offset_in_hours) }h${ dawn.getMinutes() }`;
            array_of_times[1].innerHTML = `Dusk: ${ getRealHours(dusk.getHours(), offset_in_hours) }h${ dusk.getMinutes() }`;
            current_wrapper.getElementsByClassName("location")[0].innerHTML = `${city.name}, ${city.country}`;
            current_wrapper.getElementsByClassName("current-temp")[0].innerHTML = `${Math.round(current.temp)}ºC`;
            current_wrapper.getElementsByClassName("icon")[0].src = `http://openweathermap.org/img/wn/${ current.weather[0].icon }@2x.png`;
        })
        .then(hideDialog())
}

function getRealHours(hours, offset) {
    if (hours + offset > 23) {
        return (hours + offset - 24)
    } else {
        return (hours + offset)
    }
}

function getCurrentOffset() {
    let x = new Date(Date.now());
    return x.getTimezoneOffset();
}


